(function ($) {
    var input = $('input');
    var inputText = $('textarea');
    var videoBtn = $('.video-wrap');


    $(window).on('load', function () {
        var preload = $('.prelod');
        preload.addClass('close');
        setTimeout(function () {
            preload.addClass('die');
        },610)

    });


    $('[data-scroll-go]').click(function () {
        $('html, body').animate({ scrollTop: $($(this).attr('data-scroll-go')).offset().top }, 500);
    });


    //EVENT FOR SCROLL ***********************************************
    var dataScroll = $('[data-scroll="off"]');
    $(window).on('load', function () {
        var top = $(window).scrollTop() + ($(window).height() * 0.9);
        for (var i = 0; i < dataScroll.length; i++) {
            if ($(dataScroll[i]).offset().top < top && $(dataScroll[i]).attr('data-scroll') == 'off') {
                $(dataScroll[i]).attr('data-scroll', "on");
            }
        }
        $(window).on('scroll', function () {
            var top = $(window).scrollTop() + ($(window).height() * 0.9);
            for (var i = 0; i < dataScroll.length; i++) {
                if ($(dataScroll[i]).offset().top < top && $(dataScroll[i]).attr('data-scroll') == 'off') {
                    $(dataScroll[i]).attr('data-scroll', "on");
                    dataScroll = $('[data-scroll="off"]');
                }
            }
        });
    });
//FOR THANK POPUP ***************************************************
    $(window).on('load', function () {
        if($(window).width()>768){
            $('.thank').click(function () {
                var thankImg = $(this).children('img').attr('src');
                var thankPopup = $('.thank-popup');
                var thankPopupImg = $('.thank-popup>img');
                thankPopupImg.attr('src',thankImg);

                thankPopupImg.click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });

                thankPopup.addClass('open');
                setTimeout(function () {
                    thankPopup.addClass('active');
                }, 1);


                thankPopup.click(function () {
                    thankPopup.removeClass('active');
                    setTimeout(function () {
                        thankPopup.removeClass('open');
                    }, 500);
                });

                var closeVideo = $('.close-video');
                closeVideo.click(function () {
                    thankPopup.click(function () {
                        thankPopup.removeClass('active');
                        setTimeout(function () {
                            thankPopup.removeClass('open');
                        }, 500);
                    });
                });
            });
        } else {
            var blabla = $('.video-wrap');
            $(blabla[3]).addClass('closed die');
            $(blabla[4]).addClass('closed die');
            $(blabla[5]).addClass('closed die');
        }
    });

    //SORT FOR ALL_SERVICE_PAGE
    var sortBtn = $('[data-sort2]');
    var allCategory = $('.category');
    var allCategoryWrap = $('.all-category');
    $(window).load(function () {
        allCategoryWrap.css('height', $(allCategory[0]).height());
    });
    sortBtn.click(function () {
        var thisSort = $($(this).attr('data-sort2')+"");
        allCategoryWrap.css('height', thisSort.height());
        sortBtn.removeClass('active');
        $(this).addClass('active');
        allCategory.addClass('closed');
        setTimeout(function () {
            allCategory.addClass('die');
            thisSort.removeClass('die');
            setTimeout(function () {
                thisSort.removeClass('closed');
            },30);
        },530);

    });


    //CLICK ON FILTER PORTFOLIO
    var filterBtn = $('[data-sort]'),
        allExamples = videoBtn,
        addBtn = $('[add-exapmple]');
    filterBtn.on('click', function () {
        var search = $(this).attr('data-sort') + "",
            searchArr = $(search);

        filterBtn.removeClass('active');
        $(this).addClass('active');

        allExamples.addClass('closed');
        setTimeout(function () {
            allExamples.addClass('die')
        }, 500);
        if (searchArr.length > 6) {
            addBtn.removeClass('die');
            addBtn.attr('add-exapmple', search);
            setTimeout(function () {
                searchArr.slice(0, 6).removeClass('die');
                setTimeout(function () {
                    searchArr.slice(0, 6).removeClass('closed');
                }, 100);

            }, 501);
        } else {
            addBtn.addClass('die');
            setTimeout(function () {
                searchArr.removeClass('die');
                setTimeout(function () {
                    searchArr.removeClass('closed');
                }, 100);
            }, 501);
        }
    });
    addBtn.on('click', function () {
        var search = $(this).attr('add-exapmple') + ".closed",
            searchArr = $(search);
        if (searchArr.length > 6) {
            setTimeout(function () {
                searchArr.slice(0, 6).removeClass('die');
                setTimeout(function () {
                    searchArr.slice(0, 6).removeClass('closed');
                }, 100);
            }, 501);
        } else {
            addBtn.addClass('die');
            searchArr.removeClass('die');
            setTimeout(function () {
                searchArr.removeClass('closed');
            }, 100);
        }
    });


    //NAV MENU *******************************************************
    var navBtn = $('.nav-btn');
    var navMenu = $('.nav-menu-wrap');
    var navMenuThis = $('.nav-menu');
    navBtn.click(function () {
        if (!$(this).hasClass('active')) {

            navBtn.addClass('active');
            navMenu.addClass('open');
            setTimeout(function () {
                navMenu.addClass('active');
            }, 1);

            navMenu.click(function () {
                if($(event.target).hasClass('container')){
                    navBtn.removeClass('active');
                    navMenu.removeClass('active');
                    setTimeout(function () {
                        navMenu.removeClass('open');
                    }, 500);
                }
            })
        } else {
            navBtn.removeClass('active');
            navMenu.removeClass('active');
            setTimeout(function () {
                navMenu.removeClass('open');
            }, 500);
        }
    });

    //VIDEO POPUP
    var iframeWrap = $('.video-popup');
    var iframeUrl = $('.video-popup > iframe');
    videoBtn.click(function () {
        iframeUrl.css('height', (iframeUrl.width() / 16 * 9) + "px");
        iframeUrl.attr('src', $(this).children('.video-url').text());
        iframeWrap.addClass('open');
        setTimeout(function () {
            iframeWrap.addClass('active');
        }, 1);


        iframeWrap.click(function () {
            iframeUrl.attr('src', "");
            iframeWrap.removeClass('active');

            setTimeout(function () {
                iframeWrap.removeClass('open');
            }, 500);
        });

        var closeVideo = $('.close-video');
        closeVideo.click(function () {
            iframeUrl.attr('src', "");
            iframeWrap.click(function () {
                iframeWrap.removeClass('active');
                setTimeout(function () {
                    iframeWrap.removeClass('open');
                }, 500);
            });
        });
    });

    //PoPup for call *************************************************
    var btn = $('[data-call]');
    var callClose = $('.close');
    btn.click(function () {

        var callOpen = $($(this).attr('data-call'));
        var callForm = callOpen.children('form');

        //animate open
        callOpen.addClass('open');
        setTimeout(function () {
            callOpen.addClass('active');
        }, 1);

        //animate exit
        callClose.click(function () {
            callOpen.removeClass('active');
            setTimeout(function () {
                callOpen.removeClass('open');
            }, 500);
        });
        callOpen.click(function () {
            callOpen.removeClass('active');
            setTimeout(function () {
                callOpen.removeClass('open');
            }, 500);
        });

        //cancel event in parent form
        callForm.click(function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        //cancel window scroll in open call window
        // var winScrollTop = $(window).scrollTop();
        // $(window).bind('scroll', function () {
        //     $(window).scrollTop(winScrollTop);
        // });
        // $(window).unbind('scroll');
    });

    //mail ***********************************************************
    var mailBtn = $('[data-btn]'),
        validUserName = "",
        validUserTell = "",
        validUserMassage = "",
        boolName = false,
        boolTell = false;
    inputText.blur(function () {
        validUserMassage = $(this).val()
    });
    input.blur(function () {
        var userName = /^[а-яА-ЯёЁa-zA-Z][а-яА-ЯёЁa-zA-Z\s]+$/;
        // var userEmail = /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
        var userTell = /^((\d|\+\d)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,13}$/;
        var inType = $(this).attr('name');
        var inVal = $(this).val();
        var inputWrap = $(this).closest('.input-wrap');
        if (inType == 'name') {
            if (inVal.length > 1 && userName.test(inVal)) {
                inputWrap.removeClass('error');
                boolName = true;
                validUserName = inVal;
            } else {
                inputWrap.addClass('error');
                boolName = false;
            }
        }
        // if (inType == 'email') {
        //     if (inVal.length > 1 && userEmail.test(inVal)) {
        //     inputWrap.removeClass('error');
        //     boolEmail = true;
        //     validUserEmail = inVal;
        //     } else {
        //     inputWrap.addClass('error');
        //     }
        // }
        if (inType == 'tell') {
            if (userTell.test(inVal) && inVal.length > 6) {
                inputWrap.removeClass('error');
                boolTell = true;
                validUserTell = inVal;
            } else {
                inputWrap.addClass('error');
                boolTell = false;
            }
        }
    });
    mailBtn.on('click', function () {
        var allInput = $(this).closest('form');
        var inputWrap = allInput.children('.input-wrap');
        var thisBtn = $(this);

        if (boolName && boolTell) {

            var data1 = {name: validUserName, tell: validUserTell,massage : validUserMassage};
            $.ajax({
                // url: "../sender.php",
                type: "POST",
                data: data1,
                success: function () {
                    thisBtn.addClass('die');
                    setTimeout(function () {
                        thisBtn.children('span').text('ОТПРАВЛЕНО!');
                        thisBtn.removeClass('die');
                        thisBtn.unbind('click');
                    }, 500);
                    // thisBtn.children('span').text('отправлено');
                    // thisBtn.next('.success').addClass('active');
                }
            });
        } else {
            if (!boolName) {
                $(inputWrap[0]).addClass('error');
            }
            if (!boolTell) {
                $(inputWrap[1]).addClass('error');
            }
        }
    });


    //hover for input
    input.focus(function () {
        var thisInput = $(this).parent('.input-wrap');
        thisInput.addClass('focus');
    });
    input.blur(function () {
        $(this).parent('.input-wrap').removeClass('focus');
    });


    //FOR CALCULATOR
    var cal = {
        allBtn: $('.answer'),
        go: "",
        par: "",
        parHeight:"",
        prBtn:"",
        prBlock: $('.all-result-wrap'),
        pr:$('.cena'),
        allPrice: 0,
        allAnswer:$('.all-answer'),
        qw: $('.question'),
        qwGo: "",
        qwText:1,
        clear:$('.clear'),
        allAn:$('.answer-wrap')
    };
    $(window).on('load', function () {
        cal.allAnswer.css('height', cal.allAnswer.height() + "px");
    });
    cal.allBtn.click(function () {
        cal.go = $($(this).attr('data-go'));
        cal.par = $(this).parent();
        cal.parHeight = cal.go.height();
        cal.prBtn = $(this).attr('data-price')*1||0;
        cal.qwGo = $($(this).attr('data-qw'));

        if($(this).attr('data-go') === "last"){
            console.log(cal.qwText);
            cal.allAnswer.css('height',"0px");
            cal.allPrice += cal.prBtn;
            cal.allAnswer.css('height',cal.parHeight+"px");
            cal.par.addClass('close');
            cal.prBlock.addClass('op');
            cal.qw.removeClass('active');
            setTimeout(function () {
                cal.par.addClass('die');
                cal.go.removeClass('die');
                cal.pr.text(cal.allPrice);
                cal.prBlock.removeClass('op');
                cal.qwGo.addClass('active');
                setTimeout(function () {
                    cal.go.removeClass('close');
                },30)
            },430);
        } else if($(this).hasClass('proc')){
            cal.qwText += 1;
            cal.qwGo.children('.question-number').text(cal.qwText);
            console.log(cal.prBtn||1);
            cal.allPrice -= (cal.allPrice*cal.prBtn)||0;
            cal.allAnswer.css('height',cal.parHeight+"px");
            cal.par.addClass('close');
            cal.prBlock.addClass('op');
            cal.qw.removeClass('active');
            setTimeout(function () {
                cal.par.addClass('die');
                cal.go.removeClass('die');
                cal.pr.text(cal.allPrice);
                cal.prBlock.removeClass('op');
                cal.qwGo.addClass('active');
                setTimeout(function () {
                    cal.go.removeClass('close');
                },30)
            },430);
        }else {
            cal.qwText += 1;
            cal.qwGo.children('.question-number').text(cal.qwText);
            console.log(cal.qwText);
            cal.allPrice += cal.prBtn;
            cal.allAnswer.css('height',cal.parHeight+"px");
            cal.par.addClass('close');
            cal.prBlock.addClass('op');
            cal.qw.removeClass('active');
            setTimeout(function () {
                cal.par.addClass('die');
                cal.go.removeClass('die');
                cal.pr.text(cal.allPrice);
                cal.prBlock.removeClass('op');
                cal.qwGo.addClass('active');
                setTimeout(function () {
                    cal.go.removeClass('close');
                },30)
            },430);
        }
    });
    cal.clear.click(function () {
        cal.qw.removeClass('active');
        cal.prBlock.addClass('op');
        cal.allPrice = 0;
        cal.text = 1;
        cal.qwText = 1;
        cal.allAn.addClass('close');
        cal.allAnswer.css('height',$(cal.allAn[0]).height()+"px");
        setTimeout(function () {
            $(cal.qw[0]).addClass('active');
            cal.pr.text(0);
            cal.prBlock.removeClass('op');
            cal.allAn.addClass('die');
            $(cal.allAn[0]).removeClass('die');
            setTimeout(function () {
                $(cal.allAn[0]).removeClass('close');
            },30)
        },430)
    });


})(jQuery);